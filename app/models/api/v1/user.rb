class Api::V1::User < ApplicationRecord
  has_secure_password

  validates :phone_number, uniqueness: true, phone_format: true, presence: true
  validates :email, uniqueness: true, presence: true
  validates :full_name, presence: true
  scope :recent, -> { where('created_at > ?', 1.week.ago) }
end

