class ApplicationController < ActionController::API
	include Knock::Authenticable
	#Refresh jwt to a new 24hr token
	def new_jwt
		Knock::AuthToken.new(payload:{sub: current_user.id}).token
	end
end
