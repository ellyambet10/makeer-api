Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :users
    end
  end
  root 'welcome#index'
  post 'api/v1/user_token' => 'api/v1/user_token#create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
